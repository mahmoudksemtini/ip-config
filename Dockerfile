FROM maven
EXPOSE 8080
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN mvn clean package

ENTRYPOINT java -jar target/ip-config-0.0.1-SNAPSHOT.jar
